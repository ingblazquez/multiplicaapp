
## Multiplica App Readme

Aaplicación desarrollada en ReactJS. 

### Librerías Empleadas
React JS fue la principal librería empleada para el desarrollo de esta aplicación. Adicionalmente se emplearon las siguientes librerías, las cuales fueron incluidas como dependencias de la aplicación:

- axios: Empleada para hacer las requests al endpoint indicado: `https://reqres.in/api/colors`
- copy-to-clipboard: Empleada para incorporar la función de copiar el código del color al portapapeles del usuario al clickear sobre un color.
- material-ui: Se empleó como framework de css.

### Cómo instalar dependencias y corrrer el proyecto

### `npm install`

Instala las dependencias de la aplicación en el directorio /node_modules.


### `npm start`

Inicia la aplicación en modo desarrollo.
Abrir [http://localhost:3000](http://localhost:3000) para ver en el navegador.


### URL de aplicación implementada en Heroku:
https://multiplicaapp.herokuapp.com/


### Información Adicional:

- Se empleó flexbox para el grid de colores.
- Se empleó git como control de versiones y el repositorio público se encuentra disponible en la siguiente URL: https://gitlab.com/ingblazquez/multiplicaapp