import React from "react";
import Grid from "@material-ui/core/Grid";
import Pagination from "@material-ui/lab/Pagination";
import "./pagination.css";

const CustomPagination = ({ page, setPage }) => {
 const handleChange = (event, value) => setPage(value);
 return (
  <Grid item xs={12} className="pagination-container">
   <Pagination
    count={2}
    page={page}
    className="pagination"
    size="large"
    onChange={handleChange}
   />
  </Grid>
 );
};

export default CustomPagination;
