import React from "react";
import Grid from "@material-ui/core/Grid";
import "./header.css";

export default function Header() {
 return (
  <Grid container className="header">
   <Grid item xs={12} className="header__title-containter">
    <h1 className="header__title">Colores</h1>
   </Grid>
  </Grid>
 );
}
