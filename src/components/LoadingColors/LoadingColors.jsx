import React from "react";
import Grid from "@material-ui/core/Grid";
import Skeleton from "@material-ui/lab/Skeleton";

const LoadingColors = () => (
 <Grid
  item
  xs={12}
  //  className="colors-container"
 >
  <Skeleton variant="rect" className="color" height={400} width="100%" />
 </Grid>
);

export default LoadingColors;
