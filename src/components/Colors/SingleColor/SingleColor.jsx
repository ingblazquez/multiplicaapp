import React, { useRef, Fragment } from "react";
import copy from "copy-to-clipboard";
import "./single-color.css";

const SingleColor = ({
 id,
 name,
 color,
 copied,
 year,
 pantone_value,
 handleClickColor,
}) => {
 const colorRef = useRef(null);
 const handleClick = (event) => {
  copy(color);
 };
 const handleMouseUp = (event) => {
  console.log("handleMouseUp");
  console.log("handleClickColor: ", handleClickColor);
  handleClickColor(color);
 };

 return (
  <div
   className="color"
   style={{ background: `${color}` }}
   ref={colorRef}
   id={color}
   onClick={handleClick}
   onMouseUp={handleMouseUp}
  >
   <span className="color__year">{year}</span>
   <div className="color__description">
    {copied ? (
     <span className="color__copied">¡Copiado!</span>
    ) : (
     <Fragment>
      <span className="color__name">{name}</span>
      <br />
      <span className="color__code">{color}</span>
     </Fragment>
    )}
   </div>
   <div className="color__copy">
    <span className="color__copy-text">COPIAR</span>
   </div>
   <span className="color__pantone">{pantone_value}</span>
  </div>
 );
};

export default SingleColor;
