import React from "react";
import Grid from "@material-ui/core/Grid";
import SingleColor from "./SingleColor";
import "./colors.css";

const Colors = ({ colors, handleClickColor }) => {
 return (
  <Grid item xs={12} className="colors-container">
   {colors.map((itemProps, id) => (
    <SingleColor key={id} {...itemProps} handleClickColor={handleClickColor} />
   ))}
  </Grid>
 );
};

export default Colors;
