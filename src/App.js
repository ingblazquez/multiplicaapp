import React, { useEffect, useState } from "react";
import axios from "axios";
import Layout from "./components/Layout";
import Colors from "./components/Colors";
import LoadingColors from "./components/LoadingColors";
import Pagination from "./components/Pagination";

const COLORS_URL = "https://reqres.in/api/colors";

function App() {
 const [colors, setColors] = useState([]);
 const [page, setPage] = React.useState(1);
 const [loading, setLoading] = useState(true);
 const handleClickColor = (color) => {
  setColors(
   colors.map((item) =>
    item.color === color
     ? { ...item, copied: true }
     : { ...item, copied: false }
   )
  );
 };

 useEffect(() => {
  (async () => {
   try {
    const res = await axios.get(`${COLORS_URL}?page=${page}`);
    setColors(res.data.data.map((item) => ({ ...item, copied: false })));
   } catch (err) {
    console.error(err);
   } finally {
    setLoading(false);
   }
  })();
 }, [page]);
 console.log("colors: ", colors);
 return (
  <Layout>
   {loading ? (
    <LoadingColors />
   ) : (
    <Colors colors={colors} handleClickColor={handleClickColor} />
   )}
   <Pagination page={page} setPage={setPage} />
  </Layout>
 );
}

export default App;
